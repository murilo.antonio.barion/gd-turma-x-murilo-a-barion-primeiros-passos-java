package br.com.geradordedevs;

import java.util.Scanner;

public class desafio5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Seja Bem Vindo!!!");
        System.out.print("Digite a quantidade em m³ consumida: ");
        Double m3 = in.nextDouble();

        Double conta;
        if(m3 <= 10){
            conta = 22.38;
        }else if(m3 <= 20){
            conta = 22.38 + (3.5 * (m3 - 10));
        }else if(m3 <= 30){
            conta = 22.38 + (8.75 * (m3 - 10));
        }else if(m3 <= 50){
            conta = 22.38 + (9.64 * (m3 - 10));
        }else{
            conta = 22.38 + (10.17 * (m3 - 10));
        }

        System.out.println("O valor a ser pago na conta de água é de " + conta + " reais");

    }
}

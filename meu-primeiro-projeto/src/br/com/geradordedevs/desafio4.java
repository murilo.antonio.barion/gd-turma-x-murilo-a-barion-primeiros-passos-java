package br.com.geradordedevs;

import java.util.Scanner;

public class desafio4 {
    public static void main(String[] args) {
        //declarando variavel para entrada de dados
        Scanner in = new Scanner(System.in);

        System.out.println("Seja bem vindo!!!");
        System.out.print("Digite seu nome: ");
        String nome = in.next();
        System.out.print("Digite o valor total do produto: ");
        Double preco = in.nextDouble();
        System.out.print("Digite em quantas parcelas ira dividir: ");
        Integer parcelas = in.nextInt();

        if(parcelas == 1){
            Double novo_preco = preco * 0.9;
            System.out.println("Muito obrigado " + nome + "!");
            System.out.println("Você parcelou sua compra em: 1x, no valor de: " + novo_preco + ", e o total do seu produto vai ser de : " + novo_preco + "!");
        }else if(parcelas <= 10){
            Double valor_parcela = preco/parcelas;
            System.out.println("Muito obrigado " + nome + "!");
            System.out.println("Você parcelou sua compra em: " + parcelas + "x, no valor de: " + valor_parcela + ", e o total do seu produto vai ser de : " + preco + "!");
        }else if(parcelas <= 12){
            Double novo_preco = preco * (Math.pow(2, parcelas));
            Double valor_parcela = novo_preco/parcelas;
            System.out.println("Muito obrigado " + nome + "!");
            System.out.println("Você parcelou sua compra em: " + parcelas + "x, no valor de: " + valor_parcela + ", e o total do seu produto vai ser de : " + novo_preco + "!");
        }else{
            System.out.println("Numero e parcelas não aceito!");
        }
    }
}

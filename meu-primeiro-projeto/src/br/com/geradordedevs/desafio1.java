package br.com.geradordedevs;

import java.util.Scanner;

public class desafio1 {
    public static void main(String[] args) {

        //variavel para ler algo (entrada de dados)
        Scanner in = new Scanner(System.in);

        System.out.println("Seja bem vindo!!\n");
        System.out.print("Digite o nome do aluno: ");
        String nome = in.next();
        System.out.print("Digite a primeira nota do(a) " + nome + ": ");
        Double nota1 = in.nextDouble();
        System.out.print("Digite a segunda nota do(a) " + nome + ": ");
        Double nota2 = in.nextDouble();
        System.out.print("Digite a terceira nota do(a) " + nome + ": ");
        Double nota3 = in.nextDouble();
        System.out.print("Digite a quarta nota do(a) " + nome + ": ");
        Double nota4 = in.nextDouble();

        System.out.println("\n");

        Double media = (nota1 + nota2 + nota3 + nota4)/4;

        if(media <= 4){
            System.out.println("O " + nome + " está reprovado");
        } else if (media < 6) {
            System.out.println("O " + nome + " está de recuperação");
        }else{
            System.out.println("O " + nome + " está aprovado");
        }
    }
}

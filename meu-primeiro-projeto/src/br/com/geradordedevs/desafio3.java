package br.com.geradordedevs;

import java.util.Scanner;

public class desafio3 {
    public static void main(String[] args) {
        //variavel para ler algo (entrada de dados)
        Scanner in = new Scanner(System.in);

        System.out.println("Seja bem vindo!!");
        System.out.print("Digite o sua renda mensal total: ");
        Double renda = in.nextDouble();
        System.out.print("DIgite seus gasto mensal total: ");
        Double gasto = in.nextDouble();

        if(gasto > renda){
            System.out.println("ALERTA!!!");
            System.out.println("Seus gastos são maiores que sua renda, economize mais");
        }else{
            Double pe_meia = renda * 0.1;
            if(renda < (pe_meia + gasto)){
                System.out.println("ALERTA!!!");
                System.out.println("A somatória dos gastos mais o pé de meia são maiores que sua renda, portanto o que sobrar você deverá guardar");
            }else{
                Double restante = renda - pe_meia - gasto;
                System.out.println("Você deverá guardar " + pe_meia + " para o pé de meia");
                System.out.println("Seu saldo após pagar todas as contas é: " + restante);
            }
        }

    }
}

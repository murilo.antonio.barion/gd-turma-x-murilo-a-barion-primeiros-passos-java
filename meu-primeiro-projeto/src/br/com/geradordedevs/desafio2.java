package br.com.geradordedevs;

import java.util.Scanner;

public class desafio2 {
    public static void main(String[] args) {
        //variavel para ler algo (entrada de dados)
        Scanner in = new Scanner(System.in);

        System.out.println("Seja bem vindo!!\n");
        System.out.print("Digite o nome do(a) funcionario(a) para fazer o cálculo do IMC: ");
        String nome = in.next();

        System.out.print("Digite o peso em kilogramas do(a) funcionario(a) " + nome + ": ");
        Double peso = in.nextDouble();

        System.out.print("Digite a altura em metros do(a) funcionario(a) " + nome + ": ");
        Double altura = in.nextDouble();

        Double imc = peso/Math.pow(altura,2);

        if(imc < 18.5){
            System.out.println("O(a) funcionario(a) está classificado como MAGREZA");
        }else if(imc < 25){
            System.out.println("O(a) funcionario(a) está classificado como PESO NORMAL");
        }else if(imc < 30){
            System.out.println("O(a) funcionario(a) está classificado como SOBREPESO");
        }else if(imc < 35){
            System.out.println("O(a) funcionario(a) está classificado como OBESIDADE GRAU I");
        }else if (imc < 40){
            System.out.println("O(a) funcionario(a) está classificado como OBESIDADE GRAU II");
        }else{
            System.out.println("O(a) funcionario(a) está classificado como OBESIDADE GRAU III");
        }
    }
}

package br.com.geradordedevs;


import java.util.Scanner;

public class exercicio1 {
    public static void main(String[] args) {
        //mensagem para o usuario --> sout
        System.out.println("Hello World");

        //constante
        final String VARIAVEL = "Hello people";
        //variaveis
        double var1 = 1.88;
        int inteiro = 2;
        boolean trueorfalse = true;

        //variavel para ler algo (entrada de dados)
        Scanner in = new Scanner(System.in);

        System.out.print("Informe seu nome: ");
        String nome = in.next();
        System.out.println("Seja bem vindo: " + nome + "!");
    }
}